﻿using System;

namespace Zadanie_6
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string text = "Ala ma kota, Ala ma psa ale Ala nie ma gołębia.";
            string substring = "ala";
            int counter = 0;

            string lowerCaseText = text.ToLower();
            string lowerCaseSubstring = substring.ToLower();
            int nextIndex = 0;

            while (nextIndex < lowerCaseText.Length)
            {
                nextIndex = lowerCaseText.IndexOf(lowerCaseSubstring, nextIndex);
                if (nextIndex == -1)
                {
                    break;
                }
                counter++;
                nextIndex += lowerCaseSubstring.Length;  
            }

            Console.WriteLine("\"" + substring + "\"" + " " + "occurs " + counter + " times.");
            Console.ReadLine();
        }
    }
}
