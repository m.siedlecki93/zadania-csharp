﻿using System;

namespace Zadanie_1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Random random = new Random();
            int head = 0;
            int tails = 0;
            int currentResult;

            for(int throws = 0;throws<10;throws++)
            {
                currentResult = random.Next(2);

                if (currentResult == 0)
                {
                    head++;
                }
                else if (currentResult == 1)
                {
                    tails++;
                }
                else
                {
                    Console.WriteLine("wrong roll");
                }
            }

            Console.WriteLine("head: " + head);
            Console.WriteLine("tails: " + tails);

            Console.ReadLine();
        }
    }
}
