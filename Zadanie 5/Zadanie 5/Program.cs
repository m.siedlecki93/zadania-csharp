﻿using System;
using System.IO;

namespace Zadanie_5
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string path =String.Concat(AppDomain.CurrentDomain.BaseDirectory, @"\liczby.txt");
            int value = 0;

            using (StreamWriter writer = File.CreateText(path))
            {
                writer.WriteLine("3");
                writer.WriteLine("7");
                writer.WriteLine("23");
                writer.WriteLine("0");
                writer.WriteLine("-17");
                writer.WriteLine("98");
                writer.WriteLine("12");
            }

            using (StreamReader reader = File.OpenText(path))
            {
                string number;
                while ((number = reader.ReadLine()) != null)
                {
                    value += Convert.ToInt32(number);
                }
            }
            
            Console.WriteLine(value);
            Console.ReadLine();
        }
    }
}
