﻿using System;

namespace Zadanie_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string numbers = "";
            string number = "";

            for(int i = 2000; i <= 3200; i++)
            {
                if (i % 7 == 0 && i % 5 != 0)
                {
                    number = Convert.ToString(i) + ", ";
                    numbers = numbers + number;
                }
            }

            Console.WriteLine(numbers);
            Console.ReadLine();
        }
    }
}
