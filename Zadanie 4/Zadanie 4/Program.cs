﻿using System;

namespace Zadanie_4
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string pattern = "yyyy/MM/dd hh:mm:ss";
            Console.WriteLine("Enter date: (" + pattern + ") ");
            string inputDate = Console.ReadLine();

            DateTime date = Convert.ToDateTime(inputDate);
            DateTime today = DateTime.Today;
            TimeSpan difference = today.Subtract(date);

            WriteTimeSpan(difference);
            Console.ReadLine();
        }

        public static void WriteTimeSpan(TimeSpan timeSpan)
        { 
            string d = " days, ";
            string h = " hours ";
            string m = " minutes ";
            string s = " seconds ";
            
            if (timeSpan.Days == 1)
            {
                d = " day, ";
            }
            if (timeSpan.Hours == 1)
            {
                h = " hour ";
            }
            if (timeSpan.Minutes == 1)
            {
                m = " minute ";
            }
            if (timeSpan.Seconds == 1)
            {
                s = " second ";
            }

            Console.WriteLine(timeSpan.Days + d
                + timeSpan.Hours + h
                + timeSpan.Minutes + m
                + timeSpan.Seconds + s);
        }
    }
}
