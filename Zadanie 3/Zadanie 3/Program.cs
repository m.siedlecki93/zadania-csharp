﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Zadanie_3
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            string input = "";
            string output = "";

            input = Console.ReadLine();
            List<char> inputList = new List<char>(input.ToCharArray());
            List<char> checkList = new List<char>(input.ToCharArray());
            
            inputList.Reverse();
            output = string.Join("", inputList);
            
            if (inputList.SequenceEqual(checkList))
            {
                Console.WriteLine(output + " (It's a pallindrome)");
            }
            else
            {
                Console.WriteLine(output);
            }
            
            Console.ReadLine();
        }
    }
}
