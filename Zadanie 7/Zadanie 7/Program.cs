﻿using System;
using System.Globalization;
using System.Text;

namespace Zadanie_7
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string input;
            TimeSpan time;
            string timeFormat = @"hh\:mm";


            StringBuilder outputTime = new StringBuilder("It's ");
            string[] hour = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve" };
            string[] teenMinutes = { "ten", "eleven", "twelve", "thirteen", "forteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            string[] firstDigitOfMinute = { "twenty", "thirty", "forty", "fifty" };
            string[] secondDigitOfMinute = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
            StringBuilder minutes = new StringBuilder();

            input = Console.ReadLine();

            if (TimeSpan.TryParseExact(input, timeFormat, CultureInfo.InvariantCulture, out time))
            {
                if (time.Minutes >= 10 && time.Minutes < 20)
                {
                    minutes.Append(teenMinutes[time.Minutes % 10]);
                }
                else
                {
                    if (time.Minutes == 0)
                    {
                        minutes.Append("o'clock");
                    }
                    else if (time.Minutes < 10)
                    {
                        minutes.Append("oh");
                        minutes.Append(" ");
                        minutes.Append(secondDigitOfMinute[time.Minutes % 10]);
                    }
                    else
                    {
                        minutes.Append(firstDigitOfMinute[time.Minutes / 10 - 2]);
                        minutes.Append(" ");
                        minutes.Append(secondDigitOfMinute[time.Minutes % 10]);
                    }
                }

                string endOfString = "am";
                if (time.Hours > 12)
                {
                    endOfString = "pm";
                }

                outputTime.Append(hour[time.Hours % 12 - 1]);
                outputTime.Append(" ");
                outputTime.Append(minutes);
                outputTime.Append(" ");
                outputTime.Append(endOfString);
                Console.WriteLine(outputTime);
            }
            else
            {
                Console.WriteLine("WRONG TIME FORMAT!!!");
            }
            Console.ReadLine();
        }
    }
}