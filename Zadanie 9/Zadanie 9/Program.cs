﻿using System;
using System.IO;

namespace Zadanie_9
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string path = String.Concat(AppDomain.CurrentDomain.BaseDirectory, @"\oceny.txt");

            using (StreamWriter writer = File.CreateText(path))
            {
                writer.WriteLine("2 2 3 4");
                writer.WriteLine("2");
                writer.WriteLine("1 2 3");
                writer.WriteLine("5 1 2 1 1 1");
            }

            using (StreamReader reader = File.OpenText(path))
            {
                string grade;
                while ((grade = reader.ReadLine()) != null)
                {
                    
                }
            }
        }
    }
}
