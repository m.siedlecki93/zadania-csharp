﻿using System;

namespace Zadanie_8
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string line;
            int inputNumber;
            int nextPrimeNumber;
            int previousPrimeNumber;
            string outputMessage;

            Console.WriteLine("Select natural number: ");

            while (true)
            {
                try
                {
                    line = Console.ReadLine();
                    inputNumber = Convert.ToInt32(line);
                    if (inputNumber < 0)
                    {
                        Console.WriteLine("This is not a natural number, try again: ");
                        continue;
                    }
                    break;
                }
                catch (FormatException)
                {
                    Console.WriteLine("This is not a natural number, try again: ");
                }
            }      
            

            if(IsPrime(inputNumber))
            {
                outputMessage = "Number '" + inputNumber + "' is a prime";
                Console.WriteLine(outputMessage);
                Console.ReadLine();
            }
            else
            {
                for(previousPrimeNumber = inputNumber; true; previousPrimeNumber--)
                {
                    if (IsPrime(previousPrimeNumber))
                    {
                        break;
                    }
                }
                for (nextPrimeNumber = inputNumber; true; nextPrimeNumber++)
                {
                    if (IsPrime(nextPrimeNumber))
                    {
                        break;
                    }
                }

                outputMessage = previousPrimeNumber + " < " + inputNumber + " < " + nextPrimeNumber;
                Console.WriteLine(outputMessage);
                Console.ReadLine();
            }

        }
        public static bool IsPrime(int number)
        {
            if (number == 0 || number == 1)
            {
                return true;
            }
            else
            {
                for (int divisor = 2; divisor <= number / 2; divisor++)
                {
                    if (number % divisor == 0)
                    {
                        return false;
                    }

                }
            }
            return true;
        }
    }
}
